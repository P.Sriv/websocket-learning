import { Test } from '@nestjs/testing';
import { UserController } from './UserController';
import { UserService } from '../Services/UserService';

describe('UserController', () => {
    let Controller: UserController;
    let Service: UserService;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            controllers: [UserController],
            providers: [UserService],
        }).compile();

        Service = module.get<UserService>(UserController);
        Controller = module.get<UserController>(UserController);
    });
    
    describe('Define test', () => {
        it('should define',async()=>{
            expect(Service).toBeDefined()
            expect(Controller).toBeDefined()
        })

       
       
    });
    describe('Hello test',()=>{
        it('should return an array of users', async () => {
            const result = "Hello User!";
            // jest.spyOn(Controller,'helloUser').mockImplementation(()=> result)
            jest.spyOn(Service, 'HelloUser').mockImplementation(() => result);
           
            expect(await Controller.HelloUser()).toBe(result);
        });
    })
});