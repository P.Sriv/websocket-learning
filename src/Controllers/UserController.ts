import { Body, Controller, Get ,Post} from '@nestjs/common';
import {UserService} from '../Services/UserService'
import { ApiProperty } from '@nestjs/swagger';
import { Users } from 'src/Entities/User';
class UserDTO{
    @ApiProperty()
    name:string
}
@Controller('/users')
export class UserController {
    constructor(private readonly userService: UserService){}
    

    @Get()
    async getUsers():Promise<Users[]>{
        return await this.userService.getUsers()
    }
    @Post()
    async createUser(@Body() user:Users):Promise<Users>{
        return await this.userService.createUser(user)
    }
}

