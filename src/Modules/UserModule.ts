import { Module } from '@nestjs/common';
import { UserController } from '../Controllers/UserController';
import { UserService } from 'src/Services/UserService';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from 'src/Entities/User';
@Module({
    imports:[TypeOrmModule.forFeature([Users])],
    controllers: [UserController],
    providers: [UserService],
})
export class UserModule {};