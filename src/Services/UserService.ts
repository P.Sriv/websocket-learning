import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from 'src/Entities/User';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(Users)
    private userRepository:Repository<Users>
  ){}

 
  async getUsers():Promise<Users[]>{    
    return await this.userRepository.find()
  }
  async createUser(user:Users):Promise<Users>{
    return this.userRepository.save(user)
  }
}
