import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './Modules/UserModule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users} from './Entities/User';
@Module({
  imports: [UserModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 33333,
      username: 'root',
      password: 'admin',
      database: 'mydatabase',
      entities: [Users],
      synchronize: true,
      logging:true
    })
  ],
  controllers: [AppController],
  providers: [AppService],
  
  
})
export class AppModule {}
