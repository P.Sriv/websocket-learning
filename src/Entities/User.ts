import { ApiProperty } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, Column, Timestamp } from 'typeorm';

@Entity()
export class Users  {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @Column()
    @ApiProperty()
    username: string;
    @Column()
    @ApiProperty()
    role:string
    @Column({type: 'timestamp'})
    @ApiProperty()
    created_at: Date
}
